# Diseño de HMI en Python con la Libreria TKInter
#dependencias matplotlib y pyserial
#Instalar las depedencias mediante PIP
'''
    El Programa cuenta con dos archivos, el archivo __main__.py donde se desarrolla la interfaz
    el archivo comserial.py se realiza la recepcion y conversion de datos
'''
import tkinter as tk            #Libreria principal de Tk
from tkinter import *           #interfaz grafica
from tkinter import ttk         #libreria para generar Combobox y Progressbaar
from tkinter import messagebox  #Parte de la libreria que genera mensajes informativos a usuario
from tkinter import filedialog  #Parte de la libreria para hacer dialogo de guardar como y abrir
import comserial                #Libreria desarrollada para recibir y convertir los datos
import matplotlib               #Libreria de graficas par Python
import datetime                 #Libreria para generar fechas y horas
import os                       #Libreria para obtener las directorios del computador
import shutil                   #Libreria para hacer operaciones sobre archivos como copiar
import matplotlib.pyplot as plt #Libreria pyplot de Matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk #Setea la figura y el toolbar
from matplotlib.figure import Figure    #Donde se va a mostrar los datos de la grafica
import sys                      #Para identificar si es Windows o Linux

valortanque1 = 0
valortanque2 = 0
parar = False
TITULOS =("Sans", 26)
SUBTITULOS =("Sans", 16)
LARGE_FONT = ("Sans", 12)
pagina = ''
def obtenerdatoserial():
    """
    Funcion para verificar si hay datos en el puerto serial, si hay datos
    realiza el refresco de las figuras de todos las clases, se muestrea los datos
    cada 20 segundos y el refresco de la página cada 30
    """
    if comserial.estadocomunicacion():
        if comserial.recibirdatos():
            app.after(300, app.frames[pagina].dibujar)
    app.after(20, obtenerdatoserial)

class aplicacionTanques(tk.Tk):
    """
        Clase principal, carga todas las demas ventanas y manda a llanar entre una y otra
    """
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.iconbitmap(self, default="ECOAGUA SELLO.ico")
        tk.Tk.wm_title(self, "ECOAGUA")

        container = tk.Frame(self)
        f = open("log.txt","w+b")
        f.close()
        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (bienvenidos, comunicacionSerial, HMI, eventos, graficasanterior_tanque2, graficasanterior, alarmas, graficas, graficas_tanque2):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(bienvenidos)


    def show_frame(self, cont):
        """
            Funcion para intercambiar entre ventanas
        """
        global pagina
        pagina = cont
        frame = self.frames[cont]
        frame.tkraise()

    def dibujar(self):
        """
            En este caso, dibujar no es necesario
        """
        pass



class bienvenidos(tk.Frame):
    """
        Clase bienvenidos, muestra la ventana de inicio
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent, width=300, height=300, relief='raised', borderwidth=5)
        label = tk.Label(self, text="Bienvenidos", font=SUBTITULOS)
        label.grid(columnspan = 2)
        imagen = PhotoImage(file='ECOAGUA SELLO.gif')
        imagenbienvenidos = tk.Label(self, image=imagen)
        imagenbienvenidos.image = imagen
        imagenbienvenidos.grid(columnspan=2)
        button = tk.Button(self, text="Ingresar",
                            command=lambda: controller.show_frame(comunicacionSerial)) #El lamda ayuda a llamar a otra ventana
        button.grid(row=2, column=0)

        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.grid(row=2, column=1)
    def dibujar(self):
        pass


class comunicacionSerial(tk.Frame):
    """
        Clase comunicacion serial, muestra la permite seleccionar el puerto de comunicacion en Windows,
        si se ha establecido comunicacion permite ingresar al HMI, sino muestra un mensaje informativo
    """
    combo = ''
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent, width=300, height=300) #Paracambiar tamaño
        self.label = tk.Label(self, text="Seleccione el puerto de comunicacion", font=SUBTITULOS)
        self.label.grid(columnspan=2)
        so = ''
        if sys.platform == 'win32':
            print("Corriendo dispositivo en Windows")
            so = 'win32'
        elif sys.platform == 'linux':
            print("Corriendo dispositivo en Linux")
            so = 'linux'
        elif sys.platform == 'darwin':
            print("Corriendo en un dispositivo Mac")
            so = 'darwin'
        self.combo = ttk.Combobox(self)

        if so == 'linux':
            self.combo["values"] = ['/dev/ttyACM0', '/dev/ttyACM1', '/dev/ttyUSB0', '/dev/ttyUSB1']

        elif so == 'win32':
            self.combo["values"] = ["COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8"] #Llenar el combobox con opciones
        self.combo.grid(columnspan=2)
        self.combo.bind("<<ComboboxSelected>>", self.selection_changed)

        self.button1 = Button(self, text="Ingresar",
                            command=lambda: controller.show_frame(HMI))
        self.button1.grid(row=2, column=0)
        self.button1["state"] = 'disabled'
        self.button2 = Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        self.button2.grid(row=2, column=1)

    def selection_changed(self, event):
        """
            Funcion del combobox, cuando se ha seleccionado una opcion, intenta conectar
            sino envia un mensaje de alerta
        """
        if comserial.iniciarserial(self.combo.get(),9600): #Funcion conectar
            self.button1["state"] = 'active'
            messagebox.showinfo("Éxito","Comunicacion establecida correctamente")
        else:
            messagebox.showerror("Error comunicacion", "No se puede conectar en ese puerto, verifique que sea el puerto correcto")
    def dibujar(self):
        pass

class HMI(tk.Frame):
    """
        Clase HMI, muestra todos los controles del proceso, fotos, progressbar, labels,
        y permite ver las gráficas
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent, width=1024, height=768)
        imagen = PhotoImage(file='portada2.png')
        imagenbienvenidos = tk.Label(self, image=imagen) #Muestra la imagen de biembenida
        imagenbienvenidos.image = imagen
        imagenbienvenidos.place(x=0, y=0)
        label = tk.Label(self, text="Sistema de bombeo de Tanques", font=TITULOS, bg="gray79")
        label.place(x=280, y=20)
        self.tanque1 = ttk.Progressbar(self, orient=VERTICAL, length=100, mode='determinate', maximum=1151) #Barra de progreso para simular el llenado del Tanque 1
        self.tanque1.place(x=85, y=175, height=230, width=175)
        self.tanque2 = ttk.Progressbar(self, orient=VERTICAL, length=100, mode='determinate', maximum=1151) #Barra de progreso para simular el llenado del Tanque 2
        self.tanque2.place(x=750, y=212, height=230, width=175)
        label1 = tk.Label(self, text="Hora del sistema", font=SUBTITULOS, bg="gray79")
        label1.place(x=275, y=600)
        self.tiempoactual = tk.Label(self, text="", font=LARGE_FONT , bg="gray79") #Aqui se cambia la hora del sistema
        self.tiempoactual.place(x=275, y=650)
        #Variables Tanque 1
        self.tanque_1indicador = tk.Label(self, text="Tanque 1", font=LARGE_FONT , bg="gray79")
        self.tanque_1indicador.place(x=5, y=220)
        self.tanque_1litros = tk.Label(self, text="0 Lts", font=LARGE_FONT , bg="gray79")
        self.tanque_1litros.place(x=5, y=240)
        self.tanque_1porcentaje = tk.Label(self, text="0 (%)", font=LARGE_FONT , bg="gray79")
        self.tanque_1porcentaje.place(x=144, y=282)
        self.tanque_1metros = tk.Label(self, text="0 Mts", font=LARGE_FONT , bg="gray79")
        self.tanque_1metros.place(x=5, y=280)
        self.imagen2 = PhotoImage(file='ECOAGUA SELLO.gif')
        self.imagenbienvenidos2 = tk.Label(self, image=self.imagen2)
        self.imagenbienvenidos2.place(x=5, y = 600)
        self.razonsocial = tk.Label(self, text="TRATAMIENTO Y ENVASADO DE AGUA PARA CONSUMO HUMANO ECOAGUA... ", font=LARGE_FONT , bg="gray79")
        self.razonsocial.place(x=5, y=700)
        #Variables Tanque 2
        self.tanque_2indicador = tk.Label(self, text="Tanque 2", font=LARGE_FONT , bg="gray79")
        self.tanque_2indicador.place(x=930, y=280)
        self.tanque_2litros = tk.Label(self, text="=0 Lts", font=LARGE_FONT , bg="gray79")
        self.tanque_2litros.place(x=930, y=300)
        self.tanque_2porcentaje = tk.Label(self, text=" 0 (%)", font=LARGE_FONT , bg="gray79")
        self.tanque_2porcentaje.place(x=793, y=324)
        self.tanque_2metros = tk.Label(self, text="0 Mts", font=LARGE_FONT , bg="gray79")
        self.tanque_2metros.place(x=930, y=340)
        #Variables de las electrovalvulas y motores bomba
        self.ev1color = tk.Label(self, text="EV1", font=LARGE_FONT , bg="gray79", foreground='white')
        self.ev1color.place(x=90, y=45)
        self.ev2color = tk.Label(self, text="EV2", font=LARGE_FONT , bg="gray79", foreground='white')
        self.ev2color.place(x=404, y=316)
        self.ev3color = tk.Label(self, text="EV3", font=LARGE_FONT , bg="gray79", foreground='white')
        self.ev3color.place(x=768, y=92)
        self.mt1color = tk.Label(self, text="MT1", font=LARGE_FONT , bg="gray79", foreground='white')
        self.mt1color.place(x=498, y=442)
        self.mt2color = tk.Label(self, text="MT2", font=LARGE_FONT , bg="gray79", foreground='white')
        self.mt2color.place(x=573, y=279)
        #Indicadores de error
        self.errorindicador = tk.Label(self, text="Alarma", font=LARGE_FONT , bg="gray79")
        self.errorindicador.place(x=456, y=75)
        self.errorcolor = tk.Label(self, text="Err", font=LARGE_FONT , bg="gray79", foreground='red')
        self.errorcolor.place(x=520, y=75)
        self.errorvalor = tk.Label(self, text="", font=LARGE_FONT , bg="gray79")
        self.errorvalor.place(x=456, y=100)
        #Indicadores de Proceso
        self.procesoindicador = tk.Label(self, text="Proceso", font=LARGE_FONT , bg="gray79")
        self.procesoindicador.place(x=404, y=649)
        self.procesovalor = tk.Label(self, text="", font=LARGE_FONT , bg="gray79")
        self.procesovalor.place(x=404, y=669)
        #botones de manipulacion
        botonGraficasT1 = tk.Button(self, text="Graficas Tanque 1",
                            command=lambda: controller.show_frame(graficas))
        botonGraficasT1.place(x=725, y=620)
        botonGraficasT2 = tk.Button(self, text="Graficas Tanque 2",
                            command=lambda: controller.show_frame(graficas_tanque2))
        botonGraficasT2.place(x=725, y=645)
        botonGraficasantT1 = tk.Button(self, text="Graficas Anteriores T1",
                            command=lambda: controller.show_frame(graficasanterior))
        botonGraficasantT1.place(x=725, y=670)

        botonEventos = tk.Button(self, text="Eventos",
                            command=lambda: controller.show_frame(eventos))
        botonEventos.place(x=875, y=595)
        botonAlarmas = tk.Button(self, text="Alarmas",
                            command=lambda: controller.show_frame(alarmas))
        botonAlarmas.place(x=875, y=620)
        botoncerrar = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        botoncerrar.place(x=875, y=645)
        botonGraficasantT2 = tk.Button(self, text="Graficas Anteriores T2",
                            command=lambda: controller.show_frame(graficasanterior_tanque2))
        botonGraficasantT2.place(x=875, y=670)

    def dibujar(self):
        """
            Clase dibujar, aquí cada que haya datos serial se refrescan las variables, cambian de color, cambia el texto
            y los progressbar
        """
        self.tanque1['value'] = comserial.tanque_1metros
        self.tanque2['value'] = comserial.tanque_2metros
        self.tiempoactual['text'] = str(comserial.horaactual) + ':' + str(comserial.minutoactual)
        self.tanque_1metros['text'] = str(comserial.tanque_1metros/1000) + " Mts"
        self.tanque_1litros['text'] = str(comserial.tanque_1litros) + " lts"
        self.tanque_1porcentaje['text'] = str(comserial.tanque_1porcentaje) + " (%)"
        self.tanque_2metros['text'] = str(comserial.tanque_2metros/1000) + " Mts"
        self.tanque_2litros['text'] = str(comserial.tanque_2litros) + " lts"
        self.tanque_2porcentaje['text'] = str(comserial.tanque_2porcentaje) + " (%)"
        if comserial.electrovalvula_1 == 0:
            self.ev1color['foreground'] = 'white'
            self.ev1color['bg'] = 'white'
        else:
            self.ev1color['foreground'] = 'green'
            self.ev1color['bg'] = 'green'
        if comserial.electrovalvula_2 == 0:
            self.ev2color['foreground'] = 'white'
            self.ev2color['bg'] = 'white'
        else:
            self.ev2color['foreground'] = 'green'
            self.ev2color['bg'] = 'green'
        if comserial.electrovalvula_3 == 0:
            self.ev3color['foreground'] = 'white'
            self.ev3color['bg'] = 'white'
        else:
            self.ev3color['foreground'] = 'green'
            self.ev3color['bg'] = 'green'
        if comserial.motor_1 == 0:
            self.mt1color['foreground'] = 'white'
            self.mt1color['bg'] = 'white'
        else:
            self.mt1color['foreground'] = 'green'
            self.mt1color['bg'] = 'green'
        if comserial.motor_2 == 0:
            self.mt2color['foreground'] = 'white'
            self.mt2color['bg'] = 'white'
        else:
            self.mt2color['foreground'] = 'green'
            self.mt2color['bg'] = 'green'
        if comserial.error == 0:
            self.errorcolor['foreground'] = 'white'
            self.errorcolor['bg'] = 'white'
            self.errorvalor['text'] = ""
        else:
            self.errorcolor['foreground'] = 'red'
            self.errorcolor['bg'] = 'red'
            if comserial.error == 1:
                self.errorvalor['text'] = "Nivel crítico Tanque 1"
            elif comserial.error == 2:
                self.errorvalor['text'] = "Nivel crítico Tanque 2"
            elif comserial.error == 3:
                self.errorvalor['text'] = "Nivel crítico Tanque 1 y 2"
            elif comserial.error == 4:
                self.errorvalor['text'] = "Falla Sensor 1"
            elif comserial.error == 5:
                self.errorvalor['text'] = "Falla Sensor 2"
            elif comserial.error == 6:
                self.errorvalor['text'] = "Falla Tanque 1 y Tanque 2"
            elif comserial.error == 7:
                self.errorvalor['text'] = "Presionado paro de emergencias"
        if comserial.operacion == 0:
            self.procesovalor['text'] = "Vaciado Tanques"
        elif comserial.operacion == 1:
            self.procesovalor['text'] = "Producción Automática"
        elif comserial.operacion == 2:
            self.procesovalor['text'] = "Tiempo de Reposo"
        elif comserial.operacion == 3:
            self.procesovalor['text'] = "Retrolavado"
        elif comserial.operacion == 4:
            self.procesovalor['text'] = "Sistema Detenido"

class eventos(tk.Frame):
    """
        Clase eventos, lee de un archivo de texto los eventos informativos del proceso
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Historico eventos", font=TITULOS)
        label.grid(columnspan=4)
        infile = "registro.log"         #archivo donde se encuentran los losgs
        f = open(infile,"r")            #Abro el archivo
        alarmasgeneradas = []           #Vector para guardar las ocurrencias
        g = f.readlines()               #Leo las lineas del archivo
        for line in g:
            if "INFO" in line:          #Si se encuentra el mensaje INFO almacene en el vector
                alarmasgeneradas.append(line)
        f.close()
        self.t = tk.Text(self)
        self.t.insert(0.0,alarmasgeneradas) #Muestro los datos del vector
        self.t.grid(columnspan=3)
        self.s = tk.Scrollbar(self, orient=VERTICAL, command=self.t.yview)
        self.t['yscrollcommand'] = self.s.set
        self.s.grid(column=3, row=1, sticky=(N,S))
        button1 = tk.Button(self, text="Volver",
                            command=lambda: controller.show_frame(HMI))
        button1.grid(row=2, column=0)
        button3 = tk.Button(self, text="Refrescar",
                            command=lambda: self.refrescar())
        button3.grid(row=2, column=1)
        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.grid(row=2, column=2)
    def dibujar(self):
        pass
    def refrescar(self):
        infile = "registro.log"
        f = open(infile,"r")
        alarmasgeneradas = []
        g = f.readlines()
        for line in g:
            if "INFO" in line:
                alarmasgeneradas.append(line)
        f.close()
        self.t.delete('1.0',END)
        self.t.insert(0.0,alarmasgeneradas)

class alarmas(tk.Frame):
    """
        Clase alarmas, aca se muestran todos los eventos tipo alarma que tiene el sistema,
        estos se leen del archivo.log que se crea en el directorio del proyecto
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Historico errores", font=TITULOS)
        label.grid(columnspan=4)
        infile = "registro.log"     #Ruta del archivo
        alarmasgeneradas = []       #Vector donde se almacenan las ocurrencias
        f = open(infile,"r")        #Abro el archivo
        g = f.readlines()           #Leo las lineas
        f.close()                   #Cierro el archivo para que no se quede en uso
        for line in g:
            if "ERROR " in line:
                alarmasgeneradas.append(line)   #Guardo todas las lineas que tengan "ERROR" en su contenido

        self.t = tk.Text(self)
        self.t.delete('1.0',END)
        self.t.insert(0.0,alarmasgeneradas) #Muestro en la ventana los datos
        self.t.grid(columnspan=3)
        self.s = tk.Scrollbar(self, orient=VERTICAL, command=self.t.yview)
        self.t['yscrollcommand'] = self.s.set
        self.s.grid(column=3, row=1, sticky=(N,S))
        button1 = tk.Button(self, text="Volver",
                            command=lambda: controller.show_frame(HMI))
        button1.grid(row=2, column=0)
        button3 = tk.Button(self, text="Refrescar",
                            command=lambda: self.refrescar())
        button3.grid(row=2, column=1)
        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.grid(row=2, column=2)
    def dibujar(self):
        pass
    def refrescar(self):
        infile = "registro.log"
        alarmasgeneradas = []
        f = open(infile,"r")
        g = f.readlines()
        f.close()
        for line in g:
            if "ERROR " in line:
                alarmasgeneradas.append(line)
        self.t.delete('1.0',END)
        self.t.insert(0.0,alarmasgeneradas)


class graficas(tk.Frame):
    """
        Clase graficas, aca se muestran los datos del tanque 1
    """

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        #Definicion de las figuras, limites y etiquetas
        label = tk.Label(self, text="Gráficas Tanque 1", font=TITULOS)
        label.pack(pady=10,padx=10)
        self.f = Figure(figsize=(5,5), dpi=100)
        self.a = self.f.add_subplot(511)
        self.a.set_ylabel('Tanque 1(mm)')
        self.a.set_ylim(0,1150)
        self.b = self.f.add_subplot(512, sharex = self.a)
        self.b.set_ylabel('EV1')
        self.b.set_ylim(0,1.3)
        self.c = self.f.add_subplot(513, sharex = self.b)
        self.c.set_ylabel('EV2')
        self.c.set_ylim(0,1.3)
        self.d = self.f.add_subplot(514, sharex = self.c)
        self.d.set_ylabel('EV3')
        self.d.set_ylim(0,1.3)
        self.h = self.f.add_subplot(515, sharex = self.d, sharey=self.d)
        self.h.set_ylabel('Alarma')
        self.h.set_ylim(0,1.3)
        self.f.subplots_adjust(hspace=0)
        #Definicion de los rangos y del objeto que los maneja
        self.lineaa, = self.a.plot(comserial.historicotiempo, comserial.historiconiveltanque_1, 'b')
        self.lineab, = self.b.plot(comserial.historicotiempo, comserial.historicoev1, 'm')
        self.lineac, = self.c.plot(comserial.historicotiempo, comserial.historicoev2, 'm')
        self.linead, = self.d.plot(comserial.historicotiempo, comserial.historicoev3, 'm')
        self.lineah, = self.h.plot(comserial.historicotiempo, comserial.historicoalarmatanque1, 'm')

        plt.setp([self.a.get_xticklabels() for a in self.f.axes[:-1]], visible=False)
        self.canvas = FigureCanvasTkAgg(self.f, self) #Definicion de la figura de tipo canvas para mostrar en la Ventana
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        toolbar = NavigationToolbar2Tk(self.canvas, self) #Definicion de la Barra de Herramientas para Manipualcion adicional
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        button1 = tk.Button(self, text="Regresar",
                            command=lambda: controller.show_frame(HMI))
        button1.pack()
        self.button3 = tk.Button(self, text="Parar",
                            command=self.parargrafica)
        self.button3.pack()
        self.button4 = tk.Button(self, text="Guardar",
                            command=self.guardardatos)
        self.button4.pack()

        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.pack()

    def dibujar(self):
        """
            Cuando hay datos seriales dibuja, esta limitado a 10 minutos de grafica,
            pero los datos se guardan, el eje x representa al tiempo y el eje y
            a las variables del Tanque
        """
        global parar
        if parar == False:
            minimox = comserial.historicotiempo[0]  #Como es en tiempo real establezco un nuevo minimo
            maximox = comserial.historicotiempo[len(comserial.historicotiempo)-1] #Establezco un nuevo maximo
            self.a.set_xlim(minimox, maximox) #Seteo minimo y maximo cada vez para poder ver en tiempo real
            self.b.set_xlim(minimox, maximox)
            self.c.set_xlim(minimox, maximox)
            self.d.set_xlim(minimox, maximox)
            self.lineaa.set_xdata(comserial.historicotiempo) #Establezco los datos del Eje x
            self.lineaa.set_ydata(comserial.historiconiveltanque_1) #Establezco los datos del eje y
            self.lineab.set_xdata(comserial.historicotiempo)
            self.lineab.set_ydata(comserial.historicoev1)
            self.lineac.set_xdata(comserial.historicotiempo)
            self.lineac.set_ydata(comserial.historicoev2)
            self.linead.set_xdata(comserial.historicotiempo)
            self.linead.set_ydata(comserial.historicoev3)
            self.lineah.set_xdata(comserial.historicotiempo)
            self.lineah.set_ydata(comserial.historicoalarmatanque1)
            self.canvas.draw()
    def parargrafica(self):
        """
            Para la grafica para una mejor visualizacion
        """
        global parar
        parar = not parar
        if parar:
            self.button3['text']="Iniciar"
        else:
            self.button3['text']="Parar"
    def guardardatos(self):
        """
            Funcion primordial, guarda los datos para ser analizados luego, se guarda en un archivo de texto con el siguiente informativo
            datoev1,datoev2,datoev3,datoniveltanque1,datoniveltanque2,datoalarmatanque1,datoalarmatanque2,fecha(en formato ISO)
            ej. 1,0,0,899,1049,0,0,2018-10-22T11:19:40.239946
        """
        global parar
        parar = True
        f = filedialog.asksaveasfile(mode='w+', title = "Seleccione el Archivo para guardar", filetypes = (("Archivo texto","*.txt"),("all files","*.*")))
        if f is None: # Si no hay datos, regresa
            return
        rutadirectorio  = f.name
        f.close()
        shutil.copyfile("log.txt",rutadirectorio) #Copia los datos del archivo temporal
        f = open(rutadirectorio,"a") #Abro con la opcion de seguir ingresando datos al ultimo
        parar = False
        self.vectortotal = []
        for i in range(0,len(comserial.historicoev1)): #Copio los datos que estan en la memoria del pc y les agrego
            vectorparcial = ""
            vectorparcial+=str(comserial.historicoev1[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoev2[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoev3[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historiconiveltanque_1[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historiconiveltanque_2[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoalarmatanque1[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoalarmatanque2[i])
            vectorparcial+=","
            vectorparcial+=comserial.historicotiempo[i].isoformat()
            f.write(vectorparcial) #Escribo con el formato adecuado
            f.write("\n") #salto de linea
        messagebox.showinfo("Éxito","Archivo guardado en "+ f.name)
        f.close() #Cierro el Archivo


class graficas_tanque2(tk.Frame):
    """
        Class graficas_tanque2 es casi un clon de la funcion anterior, usa los datos del tanque_2
        para  visualizar
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Gráficas Tanque 2", font=LARGE_FONT , bg="gray79")
        label.pack(pady=10,padx=10)
        self.f = Figure(figsize=(5,5), dpi=100)
        self.a = self.f.add_subplot(311)
        self.a.set_ylabel('Tanque 2(mm)')
        self.a.set_ylim(0,1150)
        self.b = self.f.add_subplot(312, sharex = self.a)
        self.b.set_ylabel('EV3')
        self.b.set_ylim(0,1.3)
        self.d = self.f.add_subplot(313, sharex = self.b, sharey = self.b)
        self.d.set_ylabel('Alarma')
        self.d.set_ylim(0,1.3)
        self.f.subplots_adjust(hspace=0)
        plt.setp([self.a.get_xticklabels() for a in self.f.axes[:-1]], visible=False)
        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.lineaa, = self.a.plot(comserial.historicotiempo, comserial.historiconiveltanque_2, 'b')
        self.lineab, = self.b.plot(comserial.historicotiempo, comserial.historicoev3, 'm')
        self.linead, = self.d.plot(comserial.historicotiempo, comserial.historicoalarmatanque2, 'm')
        toolbar = NavigationToolbar2Tk(self.canvas, self)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        button1 = tk.Button(self, text="Regresar",
                            command=lambda: controller.show_frame(HMI))
        button1.pack()
        self.button3 = tk.Button(self, text="Parar",
                            command=self.parargrafica)
        self.button3.pack()
        self.button4 = tk.Button(self, text="Guardar",
                            command=self.guardardatos)
        self.button4.pack()

        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.pack()
    def dibujar(self):
        global parar
        if parar == False:
            minimox = comserial.historicotiempo[0]
            maximox = comserial.historicotiempo[len(comserial.historicotiempo)-1]
            self.a.set_xlim(minimox, maximox)
            self.b.set_xlim(minimox, maximox)
            self.d.set_xlim(minimox, maximox)
            self.lineaa.set_xdata(comserial.historicotiempo)
            self.lineaa.set_ydata(comserial.historiconiveltanque_2)
            self.lineab.set_xdata(comserial.historicotiempo)
            self.lineab.set_ydata(comserial.historicoev3)
            self.linead.set_xdata(comserial.historicotiempo)
            self.linead.set_ydata(comserial.historicoalarmatanque2)
            self.canvas.draw()
    def parargrafica(self):
        global parar
        parar = not parar
        if parar:
            self.button3['text']="Iniciar"
        else:
            self.button3['text']="Parar"
    def guardardatos(self):
        global parar
        parar = True
        f = filedialog.asksaveasfile(mode='w+', title = "Seleccione el Archivo para guardar", filetypes = (("Archivo texto","*.txt"),("all files","*.*")))

        parar = False
        if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
            return
        rutadirectorio  = f.name
        f.close()
        shutil.copyfile("log.txt",rutadirectorio)
        f = open(rutadirectorio,"a")
        self.vectortotal = []
        for i in range(0,len(comserial.historicoev1)):
            vectorparcial = ""
            vectorparcial+=str(comserial.historicoev1[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoev2[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoev3[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historiconiveltanque_1[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historiconiveltanque_2[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoalarmatanque1[i])
            vectorparcial+=","
            vectorparcial+=str(comserial.historicoalarmatanque2[i])
            vectorparcial+=","
            vectorparcial+=comserial.historicotiempo[i].isoformat()
            f.write(vectorparcial)
            f.write("\n")
        #np.savetxt(f.name,self.vectortotal,delimiter=",")
        messagebox.showinfo("Éxito","Archivo guardado en "+ f.name)
        f.close()


class graficasanterior(tk.Frame):
    """
        Clase grafica anterior, permite ver los valores guardados con anterioridad en un archivo de texto
        para su analisis posterior, utiliza partes de la clase graficas
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Gráficas Anterior Tanque 1", font=LARGE_FONT , bg="gray79")
        label.pack(pady=10,padx=10)
        self.f = Figure(figsize=(5,5), dpi=100)
        self.a = self.f.add_subplot(511)
        self.a.set_ylim(0,1150)
        self.b = self.f.add_subplot(512, sharex = self.a)
        self.b.set_ylabel('EV1')
        self.b.set_ylim(0,1.3)
        self.c = self.f.add_subplot(513, sharex = self.b)
        self.c.set_ylabel('EV2')
        self.c.set_ylim(0,1.3)
        self.d = self.f.add_subplot(514, sharex = self.c)
        self.d.set_ylabel('EV3')
        self.d.set_ylim(0,1.3)
        self.h = self.f.add_subplot(515, sharex = self.d, sharey=self.d)
        self.h.set_ylabel('Alarma')
        self.h.set_ylim(0,1.3)
        self.f.subplots_adjust(hspace=0)
        plt.setp([self.a.get_xticklabels() for a in self.f.axes[:-1]], visible=False)
        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.lineaa, = self.a.plot(comserial.historicotiempo, comserial.historiconiveltanque_1, 'b')
        self.lineab, = self.b.plot(comserial.historicotiempo, comserial.historicoev1, 'm')
        self.lineac, = self.c.plot(comserial.historicotiempo, comserial.historicoev2, 'm')
        self.linead, = self.d.plot(comserial.historicotiempo, comserial.historicoev3, 'm')
        self.lineah, = self.h.plot(comserial.historicotiempo, comserial.historicoalarmatanque1, 'm')

        toolbar = NavigationToolbar2Tk(self.canvas, self)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        button1 = tk.Button(self, text="Regresar",
                            command=lambda: controller.show_frame(HMI))
        button1.pack()

        button1 = tk.Button(self, text="Abrir",
                            command=lambda: self.dibujar2())
        button1.pack()
        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.pack()
        self.hev1=[]
        self.hev2=[]
        self.hev3=[]
        self.hniveltanque1=[]
        self.halarmast1=[]
        self.htiempo=[]
    def dibujar(self):
        pass
    def dibujar2(self):
        #Aca hago un file dialog para abrir  archivo
        archivoentrada = filedialog.askopenfilename(initialdir = os.getcwd(),title = "Seleccione el archivo donde guardo las graficas",filetypes = (("Archivos de texto","*.txt"),("all files","*.*")))
        if archivoentrada == "":
            messagebox.showerror("Error al abrir el archivo", "Seleccione un archivo correcto")
            return
        f = open(archivoentrada)    #abro el archivo para leer
        g = f.readlines()
        for line in g:
            vectortemporal = []
            vectortemporal = line.strip('\n').split(",") #Elimino el salto de linea y transformo a vector usando la coma como separador
            self.hev1.append(float(vectortemporal[0])) #Guardo los datos en la momoria, lo hago hasta que se termine el archivo de texto
            self.hev2.append(float(vectortemporal[1]))
            self.hev3.append(float(vectortemporal[2]))
            self.hniveltanque1.append(float(vectortemporal[3]))
            self.halarmast1.append(float(vectortemporal[5]))
            self.htiempo.append(datetime.datetime.fromisoformat(vectortemporal[7])) #Convierto la fecha de formato iso
        f.close()
        minimox = self.htiempo[0]
        maximox = self.htiempo[len(self.htiempo)-1]#Establezco minimo y maximo
        self.a.set_xlim(minimox, maximox)
        self.b.set_xlim(minimox, maximox)
        self.c.set_xlim(minimox, maximox)
        self.d.set_xlim(minimox, maximox)
        self.h.set_xlim(minimox, maximox)

        self.lineaa.set_xdata(self.htiempo) #Dibujo
        self.lineaa.set_ydata(self.hniveltanque1)
        self.lineab.set_xdata(self.htiempo)
        self.lineab.set_ydata(self.hev1)
        self.lineac.set_xdata(self.htiempo)
        self.lineac.set_ydata(self.hev2)
        self.linead.set_xdata(self.htiempo)
        self.linead.set_ydata(self.hev3)
        self.lineah.set_xdata(self.htiempo)
        self.lineah.set_ydata(self.halarmast1)
        self.canvas.draw()
        self.hev1 = [] #Libero memoria
        self.hev2 = []
        self.hev3 = []
        self.hniveltanque1 = []
        self.halarmast1 = []
        self.htiempo = []
        messagebox.showinfo("Éxito","Archivo cargado correctamente")
class graficasanterior_tanque2(tk.Frame):
    """
    El mismo principio de funcionamiento que la clase graficasnterior
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Gráficas Tanque 2", font=LARGE_FONT , bg="gray79")
        label.pack(pady=10,padx=10)
        self.f = Figure(figsize=(5,5), dpi=100)
        self.a = self.f.add_subplot(411)
        self.a.set_ylabel('Tanque')
        self.a.set_ylim(0,1150)
        self.b = self.f.add_subplot(412, sharex = self.a)
        self.b.set_ylabel('EV3')
        self.b.set_ylim(0,1.3)
        self.c = self.f.add_subplot(413, sharex = self.b)
        self.c.set_ylabel('Vaciado')
        self.c.set_ylim(0,1.3)
        self.d = self.f.add_subplot(414, sharex = self.c, sharey = self.c)
        self.d.set_ylabel('Alarma')
        self.d.set_ylim(0,1.3)
        self.f.subplots_adjust(hspace=0)
        plt.setp([self.a.get_xticklabels() for a in self.f.axes[:-1]], visible=False)
        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.lineaa, = self.a.plot(comserial.historicotiempo, comserial.historiconiveltanque_2, 'b')
        self.lineab, = self.b.plot(comserial.historicotiempo, comserial.historicoev3, 'm')
        self.linead, = self.d.plot(comserial.historicotiempo, comserial.historicoalarmatanque2, 'm')
        toolbar = NavigationToolbar2Tk(self.canvas, self)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        botonAbrir = tk.Button(self, text="Abrir",
                            command=lambda: self.dibujar2())
        botonAbrir.pack()
        button1 = tk.Button(self, text="Regresar",
                            command=lambda: controller.show_frame(HMI))
        button1.pack()
        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.pack()
        self.hev3=[]
        self.hniveltanque2=[]
        self.halarmast2=[]
        self.htiempo=[]


    def dibujar2(self):
        archivoentrada = filedialog.askopenfilename(initialdir = os.getcwd(),title = "Seleccione el archivo donde guardo las graficas",filetypes = (("Archivos de texto","*.txt"),("all files","*.*")))
        if archivoentrada == "":
            messagebox.showerror("Error al abrir el archivo", "Seleccione un archivo correcto")
            return
        f = open(archivoentrada)
        g = f.readlines()
        for line in g:
            vectortemporal = []
            vectortemporal = line.strip('\n').split(",")
            self.hev3.append(float(vectortemporal[2]))
            self.hniveltanque2.append(float(vectortemporal[3]))
            self.halarmast2.append(float(vectortemporal[6]))
            self.htiempo.append(datetime.datetime.fromisoformat(vectortemporal[7]))
        f.close()
        minimox = self.htiempo[0]
        maximox = self.htiempo[len(self.htiempo)-1]
        self.a.set_xlim(minimox, maximox)
        self.b.set_xlim(minimox, maximox)
        self.d.set_xlim(minimox, maximox)

        self.lineaa.set_xdata(self.htiempo)
        self.lineaa.set_ydata(self.hniveltanque2)
        self.lineab.set_xdata(self.htiempo)
        self.lineab.set_ydata(self.hev3)
        self.linead.set_xdata(self.htiempo)
        self.linead.set_ydata(self.halarmast2)

        self.canvas.draw()
        self.hev1 = []
        self.hev2 = []
        self.hev3 = []
        self.hniveltanque2 = []
        self.halarmast2 = []
        self.htiempo = []
        messagebox.showinfo("Éxito","Archivo cargado correctamente")

    def dibujar(self):
        pass

pagina = comunicacionSerial
app = aplicacionTanques()
app.after(20, obtenerdatoserial)
app.mainloop()
