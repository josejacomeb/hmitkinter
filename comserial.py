#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Antes de correr el programa, asegurse que estÃ© instalado pip2 install pyserial
__name__ = "comserial"
import serial            #Importa la libreria serial
import logging #Libreria para hacer un registro de datos
import datetime #Libreria para sacar la fecha y la hora
import time #Delay como en Arduino
ser = serial.Serial() #Delaro un objeto serial
temporal = "";
#Datos Propios del programa Arduino
electrovalvula_1 = 0
electrovalvula_2 = 0
electrovalvula_3 = 0
motor_1 = 0
motor_2 = 0
operacion = 5
tanque_1litros=0
tanque_1porcentaje=0
tanque_1metros=0
tanque_2litros=0
tanque_2porcentaje=0
tanque_2metros=0
operacionanterior = 0
error = 5
erroranterior = 0
horaactual = 0
minutoactual = 0
hora_inicio = 0
minuto_inicio = 0
nivel_1 = 8
nivel_2 = 5
recorte = False
iniciocadena = False #Comprobar que llegue toda la cadena
fincadena = False #Comprobar que llegue toda la cadena
iterador = 0 #Para saber la posicion de los datos
comunicacionactivada = False
#Vectores donde se almacenan los datoss que llegan desdde arduino
historiconiveltanque_1 = []
historiconiveltanque_2 = []
historicoev1 = []
historicoev2 = []
historicoev3 = []
historicomotor1 = []
historicomotor2 = []
historicotiempo = []
historicoalarmatanque1 = []
historicoalarmatanque2 = []
cadenatotal = ""
anterior = 0
tiempo = 0.0
logger = logging.getLogger('registro') #Inicializo el registro
hdlr = logging.FileHandler('registro.log') #Guardo como registro.log en la carpeta del proyecto
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s') #Formato tiempo - Nivel de Evento - Mensaje
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)
tiempodatetimeactual = datetime.datetime.now() #Objetos que manejan el tiempo actual
tiempodatetimeanterior = datetime.datetime.now()
leer = 0
def iniciarserial(puerto, baudios=9600):
    """
        Clase iniciarserial, inicializa el puerto serial, devuelve true si hay conexion y
        false si no es posible establecer conexion
    """
    global ser
    global logger #uso las variables globales
    logger.info('*****INICIO DEL PROGRAMA*****')
    ser.baudrate = baudios     #Especifica la velocidad de transferencia
    ser.port = puerto #Abre el puerto serial para Arduino, siempre termina en ACM0 o ACM1
    global comunicacionactivada
    try:
        ser.open()
    except Exception as e:
        print("No se puede abrir el serial")
        logger.error('No se puede abrir el serial')
        comunicacionactivada =False
        return False
    logger.info('Puerto serial abierto')
    ser.setDTR(False) # Desactiva DTR, para iniciar el puerto serial
    time.sleep(0.022)    #Espera un tiempo
    ser.setDTR(True)  #Activa DTR para reiniciar el puerto serial
    comunicacionactivada = True
    return True

def enviardatos(dato):
    """
        Funcion por si se quiere enviar datos
    """
    print("Finalizo")

def recibirdatos():
    #Utilizo las variables globales
    global ser
    global leer
    global minutoactual, horaactual, minuto_inicio, hora_inicio
    global error, temporal, cadenatotal
    global historicoev1, historicoev2, historicoev3, historicomotor1
    global historicomotor2, historiconiveltanque_1, historiconiveltanque_2
    global historicoalarmatanque1, historicoalarmatanque2
    global historicotiempo
    global tiempo
    global tiempodatetimeactual, tiempodatetimeanterior
    w = "" #esta variable recibe los datos seriales, como tipo byte
    caracterdecodificado = "" #Variable que recibe el dato codificado de byte a char
    iniciocadenainterna = False #Debe llegar el inicio y el fin de la cadena para poder procesar
    fincadenainterna = False
    datosbuffer = ser.in_waiting    #Cuantos datos hay en el bufer
    vectorcadenatotal = []
    if datosbuffer > 1: #Espera que haya datos en el buffer para leer        datosbuffer = ser.in_waiting
        for z in range(0, datosbuffer):
            w = ser.read(1) #Recibe un datos en el buffer, cambiar el parametro de read pare recibir mas o menos datos
            try:
                caracterdecodificado = w.decode() #Hay caracteres que por el ruido no son convertibles, para eso uso try except
            except:
                print("Caracter con problemas: ", w)
            if caracterdecodificado == "*": #Primer caracter que inicia la transmision
                global iniciocadena
                iniciocadena = True
                cadenatotal = ""
                vectorcadenatotal = []
                leer = 0
            elif iniciocadena==True and not caracterdecodificado=="#": #Si inicio la cadena, sigo acumulando el valor codificado al dato tipo str
                charcadena = str(caracterdecodificado)
                cadenatotal += charcadena;
            elif iniciocadena and caracterdecodificado == "#": #Cuando llega el ultimo caracter, proceso la cadena
                global fincadena
                vectorcadenatotal = cadenatotal.split(";") #Convierto la cadena a vector, utilizando el punto y coma como delimitador
                if len(vectorcadenatotal)==32: #Solo proceso si el vector es de 32 posicion
                    fincadena = True
                    #Asigno los valores de acuerdo a la posicion del Vector
                    global electrovalvula_1, electrovalvula_2, electrovalvula_3
                    electrovalvula_1 = int(vectorcadenatotal[0])
                    electrovalvula_2 = int(vectorcadenatotal[1])
                    electrovalvula_3 = int(vectorcadenatotal[2])
                    global motor_1, motor_2
                    motor_1 = int(vectorcadenatotal[3])
                    motor_2 = int(vectorcadenatotal[4])
                    global tanque_1metros, tanque_1porcentaje, tanque_1litros
                    tanque_1metros = int(vectorcadenatotal[5])
                    tanque_1porcentaje=float(vectorcadenatotal[6])
                    tanque_1litros=float(vectorcadenatotal[7])
                    global tanque_2metros, tanque_2porcentaje, tanque_2litros
                    tanque_2metros = int(vectorcadenatotal[8])
                    tanque_2porcentaje = float(vectorcadenatotal[9])
                    tanque_2litros = float(vectorcadenatotal[10])
                    horaactual = int(vectorcadenatotal[11])
                    minutoactual = int(vectorcadenatotal[12])
                    global operacion
                    operacion = int(vectorcadenatotal[13])
                    error = int(vectorcadenatotal[14])
                    pass #Hora del Reposo 15
                    pass #Minuto de reposo 16
                    pass #Altura tanque 1 17
                    pass #Limite maximo suena alarma tanque 1 18
                    pass #Limite maximo del tanque 1 19
                    pass #Limite de seguridad maximo 20
                    pass #Limite minimo de alarma del tanque 1 21
                    pass #Altura tanque 2 22
                    pass #Limite maximo de alarma superior del tanque 2 23
                    pass #Limite maximo del tanque 2 24
                    pass #Limite de seguridad bajo del tanque 2 25
                    pass #Limite minimo de alarma del tanque 2 26
                    if iniciocadena and fincadena:
                        #Aqui guardo en los vectores los valores que obtuve
                        iniciocadena = False
                        fincadena = False
                        tiempodatetimeactual = datetime.datetime.now()
                        if (tiempodatetimeactual - tiempodatetimeanterior).microseconds > 200000: #hago que cada 200ms se actualicen los datos para no forzar a la memoria
                            historicoev1.append(electrovalvula_1)
                            historicoev2.append(electrovalvula_2)
                            historicoev3.append(electrovalvula_3)
                            historicomotor1.append(motor_1)
                            historicomotor2.append(motor_2)
                            if error == 1 or error == 3:
                                historicoalarmatanque1.append(1)
                            else:
                                historicoalarmatanque1.append(0)
                            if error == 2 or error == 3:
                                historicoalarmatanque2.append(1)
                            else:
                                historicoalarmatanque2.append(0)
                            historiconiveltanque_1.append(tanque_1metros)
                            historiconiveltanque_2.append(tanque_2metros)
                            historicotiempo.append(datetime.datetime.now())
                            global recorte
                            """
                                Parte importante, para evitar que el consumo de RAM no se dispare, remuevo los datos "antiguos"
                                de los vectores y los almaceno en un archivo temporal llamado log.txt, asi mantengo el consumo de RAM
                                constante a lo largo del tiempo y al guardar uso el archivo guardado y los datos que hay en la RAM
                            """
                            if len(historiconiveltanque_2) > 1300: #1300 datos son 10 minutos, si el vector tiene un valor mayor
                                #Saco el primer dato del vector, y lo guardo en un archivo de texto
                                recorte = True
                                archivo = open("log.txt","a")
                                vectorparcial = ""
                                vectorparcial+=str(historicoev1[0])
                                vectorparcial+=","
                                vectorparcial+=str(historicoev2[0])
                                vectorparcial+=","
                                vectorparcial+=str(historicoev3[0])
                                vectorparcial+=","
                                vectorparcial+=str(historiconiveltanque_1[0])
                                vectorparcial+=","
                                vectorparcial+=str(historiconiveltanque_2[0])
                                vectorparcial+=","
                                vectorparcial+=str(historicoalarmatanque1[0])
                                vectorparcial+=","
                                vectorparcial+=str(historicoalarmatanque2[0])
                                vectorparcial+=","
                                vectorparcial+=historicotiempo[0].isoformat()
                                historicoev1.pop(0) # Saco el primer elemento de todos los vectores
                                historicoev2.pop(0)
                                historicoev3.pop(0)
                                historicomotor1.pop(0)
                                historicomotor2.pop(0)
                                historiconiveltanque_1.pop(0)
                                historiconiveltanque_2.pop(0)
                                historicoalarmatanque2.pop(0)
                                historicoalarmatanque1.pop(0)
                                historicotiempo.pop(0)
                                archivo.write(vectorparcial) #Escribo el archivo de texto
                                archivo.write("\n")         #Salto de linea|
                                archivo.close()             #Cierro el archivo
                        tiempodatetimeanterior = datetime.datetime.now() #Obtengo el tiempo anterior
                        global logger
                        global operacionanterior, erroranterior
                        #Si hay novedades escribo en el registro de acuerdo al codigo establecido de operacion
                        if operacionanterior != operacion:
                            if operacion == 0 :
                                logger.info('Vaciado de tanques')
                            elif operacion == 1:
                                logger.info('Produccion automatica')
                            elif operacion == 2:
                                logger.info('Tiempo de Reposo')
                            elif operacion == 3:
                                logger.info('Retrolavado')
                            elif operacion == 4:
                                logger.info('Sistema detenido')
                        #Si hay novedades escribo en el registro de acuerdo al codigo establecido al evento
                        if erroranterior != error:
                            if error == 0:
                                pass #Preguntar si va
                            elif error == 1:
                                logger.error("Nivel crÃ­tico del tanque1")
                            elif error == 2:
                                logger.error("Nivel crÃ­tico del tanque2")
                            elif error == 3:
                                logger.error("Nivel crÃ­tico T1 y T2")
                            elif error == 4:
                                logger.error("Falla sensor 1")
                            elif error == 5:
                                logger.error("Falla sensor 2")
                            elif error == 6:
                                logger.error("Falla T1 y T2")
                            elif error == 7:
                                logger.error("Paro de emergencia")

                        erroranterior = error
                        operacionanterior = operacion
    global anterior
    datos = False
    if anterior < len(historicoev1) or recorte==True: #Solo si hay valores nuevos envio a refrescar el sitio.
            datos = True
    anterior = len(historicoev1)
    return datos
def estadocomunicacion():
    return comunicacionactivada
def terminarserial(): #Funcion para terminar el serial
    ser.close()
