# DiseÃ±o de HMI en Python con la Libreria TKInter
#dependencias matplotlib y pyserial
#Instalar las depedencias mediante PIP
import tkinter as tk            #Libreria principal de Tk
from tkinter import *           #interfaz grafica
from tkinter import ttk         #libreria para generar Combobox y Progressbaar
from tkinter import messagebox  #Parte de la libreria que genera mensajes informativos a usuario
from tkinter import filedialog  #Parte de la libreria para hacer dialogo de guardar como y abrir
import matplotlib               #Libreria de graficas par Python
import datetime                 #Libreria para generar fechas y horas
import os                       #Libreria para obtener las directorios del computador
import shutil                   #Libreria para hacer operaciones sobre archivos como copiar
import matplotlib.pyplot as plt #Libreria pyplot de Matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk #Setea la figura y el toolbar
from matplotlib.figure import Figure    #Donde se va a mostrar los datos de la grafica
import sys                      #Para identificar si es Windows o Linux

valortanque1 = 0
valortanque2 = 0
parar = False
TITULOS =("Sans", 26)
SUBTITULOS =("Sans", 16)
LARGE_FONT = ("Sans", 12)
pagina = ''

class aplicacionTanques(tk.Tk):
    """
        Clase principal, carga todas las demas ventanas y manda a llanar entre una y otra
    """
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.iconbitmap(self, default="ECOAGUA SELLO.ico")
        tk.Tk.wm_title(self, "ECOAGUA")

        container = tk.Frame(self)
        f = open("log.txt","w+b")
        f.close()
        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (bienvenidos, HMI, graficasanterior_tanque2, graficasanterior):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(bienvenidos)


    def show_frame(self, cont):
        """
            Funcion para intercambiar entre ventanas
        """
        global pagina
        pagina = cont
        frame = self.frames[cont]
        frame.tkraise()

    def dibujar(self):
        """
            En este caso, dibujar no es necesario
        """
        pass



class bienvenidos(tk.Frame):
    """
        Clase bienvenidos, muestra la ventana de inicio
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent, width=300, height=300, relief='raised', borderwidth=5)
        label = tk.Label(self, text="Bienvenidos", font=SUBTITULOS)
        label.grid(columnspan = 2)
        imagen = PhotoImage(file='ECOAGUA SELLO.gif')
        imagenbienvenidos = tk.Label(self, image=imagen)
        imagenbienvenidos.image = imagen
        imagenbienvenidos.grid(columnspan=2)
        button = tk.Button(self, text="Ingresar",
                            command=lambda: controller.show_frame(HMI)) #El lamda ayuda a llamar a otra ventana
        button.grid(row=2, column=0)

        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.grid(row=2, column=1)
    def dibujar(self):
        pass

class HMI(tk.Frame):
    """
        Clase HMI, muestra todos los controles del proceso, fotos, progressbar, labels,
        y permite ver las grÃ¡ficas
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent, width=1024, height=768)
        label = tk.Label(self, text="Sistema de bombeo de Tanques", font=TITULOS, bg="gray79")
        label.grid(columnspan = 2)
        #botones de manipulacion
        botonGraficasantT1 = tk.Button(self, text="Graficas Anteriores T1",
                            command=lambda: controller.show_frame(graficasanterior))
        botonGraficasantT1.grid(row=1, column=0)

        botonGraficasantT2 = tk.Button(self, text="Graficas Anteriores T2",
                            command=lambda: controller.show_frame(graficasanterior_tanque2))
        botonGraficasantT2.grid(row=1, column=1)

    def dibujar(self):
        """
            Clase dibujar, aquÃ­ cada que haya datos serial se refrescan las variables, cambian de color, cambia el texto
            y los progressbar
        """
        pass

class graficasanterior(tk.Frame):
    """
        Clase grafica anterior, permite ver los valores guardados con anterioridad en un archivo de texto
        para su analisis posterior, utiliza partes de la clase graficas
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="GrÃ¡ficas Anterior Tanque 1", font=LARGE_FONT , bg="gray79")
        label.pack(pady=10,padx=10)
        self.f = Figure(figsize=(5,5), dpi=100)
        self.a = self.f.add_subplot(511)
        self.a.set_ylim(0,1150)
        self.b = self.f.add_subplot(512, sharex = self.a)
        self.b.set_ylabel('EV1')
        self.b.set_ylim(0,1.3)
        self.c = self.f.add_subplot(513, sharex = self.b)
        self.c.set_ylabel('EV2')
        self.c.set_ylim(0,1.3)
        self.d = self.f.add_subplot(514, sharex = self.c)
        self.d.set_ylabel('EV3')
        self.d.set_ylim(0,1.3)
        self.h = self.f.add_subplot(515, sharex = self.d, sharey=self.d)
        self.h.set_ylabel('Alarma')
        self.h.set_ylim(0,1.3)
        self.f.subplots_adjust(hspace=0)
        plt.setp([self.a.get_xticklabels() for a in self.f.axes[:-1]], visible=False)
        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.lineaa, = self.a.plot([ datetime.datetime.now()],[1] , 'b')
        self.lineab, = self.b.plot([ datetime.datetime.now()],[1] , 'm')
        self.lineac, = self.c.plot([ datetime.datetime.now()],[1] , 'm')
        self.linead, = self.d.plot([ datetime.datetime.now()],[1] , 'm')
        self.lineah, = self.h.plot([ datetime.datetime.now()],[1] , 'm')

        toolbar = NavigationToolbar2Tk(self.canvas, self)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        button1 = tk.Button(self, text="Regresar",
                            command=lambda: controller.show_frame(HMI))
        button1.pack()

        button1 = tk.Button(self, text="Abrir",
                            command=lambda: self.dibujar2())
        button1.pack()
        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.pack()
        self.hev1=[]
        self.hev2=[]
        self.hev3=[]
        self.hniveltanque1=[]
        self.halarmast1=[]
        self.htiempo=[]
    def dibujar(self):
        pass

    def dibujar2(self):
        #Aca hago un file dialog para abrir  archivo
        archivoentrada = filedialog.askopenfilename(initialdir = os.getcwd(),title = "Seleccione el archivo donde guardo las graficas",filetypes = (("Archivos de texto","*.txt"),("all files","*.*")))
        if archivoentrada == "":
            messagebox.showerror("Error al abrir el archivo", "Seleccione un archivo correcto")
            return
        f = open(archivoentrada)    #abro el archivo para leer
        g = f.readlines()
        for line in g:
            vectortemporal = []
            vectortemporal = line.strip('\n').split(",") #Elimino el salto de linea y transformo a vector usando la coma como separador
            self.hev1.append(float(vectortemporal[0])) #Guardo los datos en la momoria, lo hago hasta que se termine el archivo de texto
            self.hev2.append(float(vectortemporal[1]))
            self.hev3.append(float(vectortemporal[2]))
            self.hniveltanque1.append(float(vectortemporal[3]))
            self.halarmast1.append(float(vectortemporal[5]))
            self.htiempo.append(datetime.datetime.fromisoformat(vectortemporal[7])) #Convierto la fecha de formato iso
        f.close()
        minimox = self.htiempo[0]
        maximox = self.htiempo[len(self.htiempo)-1]#Establezco minimo y maximo
        self.a.set_xlim(minimox, maximox)
        self.b.set_xlim(minimox, maximox)
        self.c.set_xlim(minimox, maximox)
        self.d.set_xlim(minimox, maximox)
        self.h.set_xlim(minimox, maximox)

        self.lineaa.set_xdata(self.htiempo) #Dibujo
        self.lineaa.set_ydata(self.hniveltanque1)
        self.lineab.set_xdata(self.htiempo)
        self.lineab.set_ydata(self.hev1)
        self.lineac.set_xdata(self.htiempo)
        self.lineac.set_ydata(self.hev2)
        self.linead.set_xdata(self.htiempo)
        self.linead.set_ydata(self.hev3)
        self.lineah.set_xdata(self.htiempo)
        self.lineah.set_ydata(self.halarmast1)
        self.canvas.draw()
        self.hev1 = [] #Libero memoria
        self.hev2 = []
        self.hev3 = []
        self.hniveltanque1 = []
        self.halarmast1 = []
        self.htiempo = []
        messagebox.showinfo("Ãxito","Archivo cargado correctamente")
class graficasanterior_tanque2(tk.Frame):
    """
    El mismo principio de funcionamiento que la clase graficasnterior
    """
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="GrÃ¡ficas Tanque 2", font=LARGE_FONT , bg="gray79")
        label.pack(pady=10,padx=10)
        self.f = Figure(figsize=(5,5), dpi=100)
        self.a = self.f.add_subplot(411)
        self.a.set_ylabel('Tanque')
        self.a.set_ylim(0,1150)
        self.b = self.f.add_subplot(412, sharex = self.a)
        self.b.set_ylabel('EV3')
        self.b.set_ylim(0,1.3)
        self.c = self.f.add_subplot(413, sharex = self.b)
        self.c.set_ylabel('Vaciado')
        self.c.set_ylim(0,1.3)
        self.d = self.f.add_subplot(414, sharex = self.c, sharey = self.c)
        self.d.set_ylabel('Alarma')
        self.d.set_ylim(0,1.3)
        self.f.subplots_adjust(hspace=0)
        plt.setp([self.a.get_xticklabels() for a in self.f.axes[:-1]], visible=False)
        self.canvas = FigureCanvasTkAgg(self.f, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.lineaa, = self.a.plot([datetime.datetime.now()], [1], 'b')
        self.lineab, = self.b.plot([datetime.datetime.now()], [1], 'm')
        self.linead, = self.d.plot([datetime.datetime.now()], [1], 'm')
        toolbar = NavigationToolbar2Tk(self.canvas, self)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        botonAbrir = tk.Button(self, text="Abrir",
                            command=lambda: self.dibujar2())
        botonAbrir.pack()
        button1 = tk.Button(self, text="Regresar",
                            command=lambda: controller.show_frame(HMI))
        button1.pack()
        button2 = tk.Button(self, text="Cerrar",
                            command=lambda: self.destroy())
        button2.pack()
        self.hev3=[]
        self.hniveltanque2=[]
        self.halarmast2=[]
        self.htiempo=[]


    def dibujar2(self):
        archivoentrada = filedialog.askopenfilename(initialdir = os.getcwd(),title = "Seleccione el archivo donde guardo las graficas",filetypes = (("Archivos de texto","*.txt"),("all files","*.*")))
        if archivoentrada == "":
            messagebox.showerror("Error al abrir el archivo", "Seleccione un archivo correcto")
            return
        f = open(archivoentrada)
        g = f.readlines()
        for line in g:
            vectortemporal = []
            vectortemporal = line.strip('\n').split(",")
            self.hev3.append(float(vectortemporal[2]))
            self.hniveltanque2.append(float(vectortemporal[3]))
            self.halarmast2.append(float(vectortemporal[6]))
            self.htiempo.append(datetime.datetime.fromisoformat(vectortemporal[7]))
        f.close()
        minimox = self.htiempo[0]
        maximox = self.htiempo[len(self.htiempo)-1]
        self.a.set_xlim(minimox, maximox)
        self.b.set_xlim(minimox, maximox)
        self.d.set_xlim(minimox, maximox)

        self.lineaa.set_xdata(self.htiempo)
        self.lineaa.set_ydata(self.hniveltanque2)
        self.lineab.set_xdata(self.htiempo)
        self.lineab.set_ydata(self.hev3)
        self.linead.set_xdata(self.htiempo)
        self.linead.set_ydata(self.halarmast2)

        self.canvas.draw()
        self.hev3 = []
        self.hniveltanque2 = []
        self.halarmast2 = []
        self.htiempo = []
        messagebox.showinfo("Ãxito","Archivo cargado correctamente")

    def dibujar(self):
        pass

app = aplicacionTanques()
app.mainloop()
